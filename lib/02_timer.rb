class Timer
 attr_accessor :seconds

  def initialize ()
    @seconds = 0
  end

  public
  def seconds
    @seconds
  end

  def time_string
    s_final = @seconds % 60
    m_final = (@seconds / 60) % 60
    h_final = @seconds / (60 * 60)

    padded(h_final) + ":" + padded(m_final) + ":" + padded(s_final)
  end

  private 
  def padded(time)
      if time < 10
        "0" + time.to_s
      else
        time.to_s
      end
  end

end
