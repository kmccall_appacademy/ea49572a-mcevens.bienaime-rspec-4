class Temperature
  # TODO: your code goes here!
  attr_accessor = :fahrenheit , :celsius
  def initialize(hash={})
    @fahrenheit = hash[:f]
    @celsius = hash[:c]
  end


  def in_fahrenheit
    if @fahrenheit == nil
        @celsius * 9/5.0 + 32
    else
      @fahrenheit
     end
  end


  def in_celsius
    if @celsius == nil
         ( @fahrenheit - 32  )  * 5/9.0
    else
      @celsius
    end
  end

  def self.from_celsius (temp_celc)
     @celsius = temp_celc
     Temperature.new(:c => temp_celc)
  end

  def self.from_fahrenheit(temp_far)
      @fahrenheit = temp_far
      Temperature.new(:f => temp_far)
  end

  def self.ftoc(temp_far)
    (temp_far - 32) * 5/9.0
  end

  def self.ctof(temp_celc)
    temp_celc * 9/5.0 + 32
  end

end

class Celsius  < Temperature
    attr_accessor :celsius

    def initialize(celsius)
      @celsius = celsius
    end
end


class Fahrenheit < Temperature
    attr_accessor :fahrenheit

    def initialize(fahrenheit)
      @fahrenheit = fahrenheit
    end

end
