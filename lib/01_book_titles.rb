class Book
  # TODO: your code goes here!
  attr_accessor :title
  BLACKLIST = ["a","and","an","in","of","the"]

  def initialize()
      @title = ""
  end

 public
 def title
    @title.split(" ").map.with_index {|el,idx| capitalize_word(el,idx) }.join(" ")
 end

 private
 def capitalize_word(word,idx)
    return word.capitalize if idx == 0
    if BLACKLIST.include?(word)
      return word
    else
      return word.capitalize
    end
 end

end
