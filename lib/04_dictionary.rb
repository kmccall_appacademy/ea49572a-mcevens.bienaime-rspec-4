class Dictionary
  # TODO: your code goes here!
  attr_accessor :entries,:keywords

  def initialize
      @entries = Hash.new(nil)
  end

  def add(input)
    if  input.instance_of? String
        @entries[input] = nil
    elsif input.instance_of?Hash
      (input).each {|k,v=nil|
        @entries[k] = v
      }
    end
  end

  def include?(str)
      @entries.keys.include?(str)
  end

  def find(str)
    @entries.select do |k,v|
        k.include?(str)
    end
  end

  def entries
    @entries
  end

  def keywords
    @entries.keys.sort_by { |el| el   }
  end

  def printable
     result = ""
     @entries.sort_by{|k,v| k}.each do |k,v|
         if result == ""
             result = "[#{k}] \"#{v}\""
          else
            result += "\n" + "[#{k}] \"#{v}\""
         end
     end
     result
  end

end
